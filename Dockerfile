FROM node:14
WORKDIR /app
RUN npm install
COPY . .
CMD node server.js
EXPOSE 8000